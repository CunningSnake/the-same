var size = 16,
    levelSize = 32,
    chainLen = 3,
    blocks = [],
    chain = [],
    toFall = [],
    scoreBoard = {
        score: 0,
        element: 0
    };

document.title = "THE SAME";

scoreBoard.score = 0;
scoreBoard.element = document.createElement("h1");
scoreBoard.element.style.position = "absolute";
scoreBoard.element.style.left = levelSize * size + 64;
scoreBoard.element.style.top = 64;
scoreBoard.element.textContent = scoreBoard.score;
scoreBoard.addScore = function (add) {
    scoreBoard.score += add;
    scoreBoard.element.textContent = scoreBoard.score;
};
document.body.appendChild(scoreBoard.element);

function block(x, y, color) {

    this.x = x;
    this.y = y;
    this.color = color;
    this.blockElement = document.createElement("abbr");
    this.blockElement.title = this.x + " " + this.y;
    this.blockElement.style.position = "absolute";
    this.blockElement.style.left = this.x * size;
    this.blockElement.style.top = this.y * size;
    this.blockElement.style.backgroundColor = this.color;
    this.blockElement.style.width = size;
    this.blockElement.style.height = size;
    this.blockElement.style.display = "block";
    this.target = false;

    this.fall = function () {
        if (blocks[this.x][this.y + 1] == null && (this.y != levelSize - 1)) {
            var prev = this.y;
            y++;
            this.y++;
            this.blockElement.style.top = this.y * size;
            this.blockElement.title = this.x + " " + this.y;
            blocks[this.x][this.y] = this;
            blocks[this.x][prev] = null;
            this.fall();
        }
    };
    this.moveLeft = function () {
        var prev = this.x;
        this.x--;
        x--;
        this.blockElement.style.left = this.x * size;
        this.blockElement.title = this.x + " " + this.y;
        blocks[this.x][this.y] = this;
        blocks[prev][this.y] = null;

        if (blocks[prev][this.y - 1]) {
            blocks[prev][this.y - 1].moveLeft();
        }

        if ((this.y == levelSize - 1) && this.x != 0 && !blocks[this.x - 1][levelSize - 1]) {
            this.moveLeft();
        }
    };
    this.scan = function (dir) {
        this.target = true;
        chain.push(this);
        if (y > 0 && dir != 0) {
            if (blocks[x][y - 1] && (blocks[x][y - 1].blockElement.style.backgroundColor == color) && !blocks[x][y - 1].target) {
                blocks[x][y - 1].scan(2);
            }
        }
        if (y < levelSize - 1 && dir != 2) {
            if (blocks[x][y + 1] && (blocks[x][y + 1].blockElement.style.backgroundColor == color) && !blocks[x][y + 1].target) {
                blocks[x][y + 1].scan(0);
            }
        }

        if (x > 0 && dir != 3) {
            if (blocks[x - 1][y] && (blocks[x - 1][y].blockElement.style.backgroundColor == color) && !blocks[x - 1][y].target) {
                blocks[x - 1][y].scan(1);
            }
        }

        if (x < levelSize - 1 && dir != 1) {
            if (blocks[x + 1][y] && (blocks[x + 1][y].blockElement.style.backgroundColor == color) && !blocks[x + 1][y].target) {
                blocks[x + 1][y].scan(3);
            }
        }
    }

    this.blockElement.onmousedown = function () {
        blocks[x][y].scan();
        if (chain.length >= chainLen) {
            var minX = levelSize,
                maxX = 0,
                maxY = 0;
            for (var i = 0; i < chain.length; i++) {
                var z = chain[i];
                minX = Math.min(minX, z.x);
                maxX = Math.max(maxX, z.x);
                maxY = Math.max(maxY, z.y);
                z.blockElement.parentNode.removeChild(z.blockElement);
                blocks[z.x][z.y] = null;
            }
            scoreBoard.addScore(Math.round(Math.pow(chain.length, 2) / 3));
            chain = [];
            refresh(minX, maxX, maxY - 1);
        } else {
            for (var i = 0; i < chain.length; i++) {
                var z = chain[i];
                blocks[z.x][z.y].target = false;
            }
        }
        chain = [];
    };


    document.body.appendChild(this.blockElement);

}

for (var x = 0; x < levelSize; x++) {
    blocks[x] = [];
    for (var y = 0; y < levelSize; y++) {
        var rand = Math.random(),
            color = "";
        if (rand < 0.2) {
            color = "red";
        } else if (rand < 0.4) {
            color = "blue";
        } else if (rand < 0.6) {
            color = "green";
        } else if (rand < 0.8) {
            color = "yellow";
        } else if (rand < 1) {
            color = "orange";
        }
        blocks[x][y] = new block(x, y, color);
    }
}

function refresh(minX, maxX, maxY) {
    for (var y = maxY; y >= 0; y--) {
        for (var x = maxX; x >= minX; x--) {
            if (blocks[x][y] != null)
                blocks[x][y].fall();
        }
    }
    for (var x = 0; x < levelSize; x++) {
        if (!blocks[x][levelSize - 1]) {
            moveLeft(x + 1);
        }
    }

    for (var x = 0; x < levelSize; x++) {
        for (var y = 0; y < levelSize; y++) {
            if (blocks[x] && blocks[x][y]) {
                blocks[x][y].scan();
                if (chain.length >= chainLen) {

                    y = levelSize + 1;
                    x = levelSize + 1;
                }
                for (var i = 0; i < chain.length; i++) {
                    var z = chain[i];
                    blocks[z.x][z.y].target = false;
                }
                chain = [];

            }
        }
    }
    if (x != levelSize + 2)
        alert("YOU LOSE!\nScore: " + scoreBoard.score);

}

function moveLeft(colX) {
    if (blocks[colX] && blocks[colX][levelSize - 1])
        blocks[colX][levelSize - 1].moveLeft();

}